package dev.yorkecao.demo.mybatis.entity;

import lombok.Data;

@Data
public class Demo {

    private String id;

    private String name;
}
