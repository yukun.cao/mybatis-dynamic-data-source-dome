package dev.yorkecao.demo.mybatis.mapper;

import dev.yorkecao.demo.mybatis.annotation.DynamicDataSource;
import dev.yorkecao.demo.mybatis.entity.Demo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface DemoMapper {

    @DynamicDataSource({"dev:ds1", "test:ds2"})
    Demo selectDemo(@Param("id") String id);
}
