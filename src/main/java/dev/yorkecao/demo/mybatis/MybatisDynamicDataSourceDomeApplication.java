package dev.yorkecao.demo.mybatis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@EnableAspectJAutoProxy(exposeProxy = true, proxyTargetClass = true)
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class MybatisDynamicDataSourceDomeApplication {

    public static void main(String[] args) {
        SpringApplication.run(MybatisDynamicDataSourceDomeApplication.class, args);
    }

}
