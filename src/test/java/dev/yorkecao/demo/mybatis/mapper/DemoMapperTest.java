package dev.yorkecao.demo.mybatis.mapper;

import dev.yorkecao.demo.mybatis.config.DemoConfig;
import dev.yorkecao.demo.mybatis.entity.Demo;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@Slf4j
@SpringBootTest
@RunWith(SpringRunner.class)
public class DemoMapperTest {

    private static final String ENV = "test";

    @Autowired
    private DemoConfig demoConfig;
    @Autowired
    private DemoMapper demoMapper;


    @Before
    public void setEnv() {
        demoConfig.setEnv(ENV);
    }

    @Test
    public void selectDemo() {
        Demo demo = demoMapper.selectDemo("1");

        Assert.assertTrue(ENV.equals(demo.getName()));
    }
}